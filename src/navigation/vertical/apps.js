import { Mail, MessageSquare, CheckSquare, Calendar, FileText, Circle, ShoppingCart, User } from 'react-feather'

export default [
  {
    id: 'grafik-analis',
    title: 'Grafik Analis',
    icon: <Mail size={20} />,
    navLink: '/grafik-analis'
  }
]
