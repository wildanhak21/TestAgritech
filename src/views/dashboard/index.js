
import { kFormatter } from '@utils'
import { Users, Terminal } from 'react-feather'
import ph from '../../assets/images/ph.png'
import { Row, Col } from 'reactstrap'
import SubscribersGained from '@src/views/ui-elements/cards/statistics/SubscribersGained'

const DashboardHome = () => {
    return (
      <div id='dashboard-home'>
        <Row className='match-height'>
          <Col lg='3' sm='6'>
            <SubscribersGained text={'PH Level'} kFormatter={kFormatter} icon={<img src={ph} width={30}/>} />
          </Col>
          <Col lg='3' sm='6'>
            <SubscribersGained text={'DO'} kFormatter={kFormatter} icon={<img src={ph} width={30}/>} />
          </Col>
          <Col lg='3' sm='6'>
            <SubscribersGained text={'Turbidity'} kFormatter={kFormatter} icon={<img src={ph} width={30}/>} />
          </Col>
          <Col lg='3' sm='6'>
            <SubscribersGained text={'Temperature'} kFormatter={kFormatter} icon={<img src={ph} width={30}/>} />
          </Col>
        </Row>
      </div>
    )
  }
  
  export default DashboardHome
  