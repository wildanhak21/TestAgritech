import { useEffect, useState } from 'react'
import axios from 'axios'
import StatsWithAreaChart from '@components/widgets/stats/StatsWithAreaChart'

const SubscribersGained = ({ kFormatter, text, icon }) => {
  const [data, setData] = useState(null)

  useEffect(() => {
    axios.get('/card/card-statistics/subscribers').then(res => setData(res.data))
  }, [])

  return data !== null ? (
    <StatsWithAreaChart
      icon={icon}
      color='primary'
      stats={text}
      statTitle={kFormatter(data.analyticsData.subscribers)}
      series={data.series}
      type='area'
    />
  ) : null
}

export default SubscribersGained
